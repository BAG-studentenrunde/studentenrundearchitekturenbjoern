﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Net.Client;
using static client.Greeter;

namespace client
{
    class Program
    {
        private static GreeterClient client;
        private static string name;
        private static int currentMessageId;

        static async Task Main(string[] args)
        {
            name = "TestClient 1";
            // The port number(5001) must match the port of the gRPC server.
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            client =  new Greeter.GreeterClient(channel);
            var reply = await client.SayHelloAsync(
                              new HelloRequest { Name = name });
            Console.WriteLine("Greeting: " + reply.Message);
            currentMessageId = reply.Id;

            Task t1 = new Task(RunReceiver);
            t1.Start();

            RunSender();
        }

        static async void RunReceiver() {
            var msgReply = await client.GetMessagesAsync(
                                    new MessageRequest {
                                        CurrentId = currentMessageId
                                    });
            while(msgReply.Message_ != "") {
                Console.WriteLine("Message received from " + msgReply.Name + ": " + msgReply.Message_);
                Thread.Sleep(1000);
                msgReply = await client.GetMessagesAsync(
                                    new MessageRequest {
                                        CurrentId = currentMessageId
                                    });
            }
        }

        static void RunSender() {
            Console.WriteLine("Sender started, type end to stop.");

            Console.WriteLine("Message:");
            string message = Console.ReadLine();
            while(message.ToLower() != "end") {
                client.SendMessage(new Message {
                    Message_ = message,
                    Name = name
                });

                message = Console.ReadLine();
            }
        }
    }
}
