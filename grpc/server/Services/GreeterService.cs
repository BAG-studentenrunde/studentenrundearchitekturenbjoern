using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace grpc
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
            return Task.FromResult(new HelloReply
            {
                Message = "Hello " + request.Name,
                Id = getHighestNumber()
            });
        }

        public override Task<Message> GetMessages(MessageRequest request, ServerCallContext context)
        {
            Message message = null;
            bool dequeued = tryLoadFromFileSystem(request.CurrentId + 1, out message);

            if(dequeued) {
                return Task.FromResult(message);
            } else {
                return Task.FromResult(new Message {
                    Message_ = "",
                    Name = ""
                });
            }
        }

        public override Task<Empty> SendMessage(Message request, ServerCallContext context)
        {
            saveToFileSystem(request);
            return Task.FromResult(new Empty
            {
            });
        }

        private int saveToFileSystem(Message m) {
            int numberToSave = getHighestNumber();

            File.WriteAllText((numberToSave + 1) + ".txt", JsonSerializer.Serialize(m));

            return 0;
        }

        private int getHighestNumber()
        {
            IEnumerable<string> files = Directory.GetFiles("./").Where(x => x.EndsWith(".txt"));

            int numberToSave = 0;
            if(files.Count() > 0) {
                foreach(string f in files) {
                    //GET HIGHEST NUMBER
                    string number = f.Replace("./", "").Replace(".txt", "");
                    int outputnumber = 0;
                    if(int.TryParse(number, out outputnumber)){
                        if(outputnumber > numberToSave) {
                            numberToSave = outputnumber;
                        }
                    }
                }
            }

            return numberToSave;
        }

        private bool tryLoadFromFileSystem(int fileToLoad, out Message m) {
            if(File.Exists(fileToLoad+".txt")) {
                m = JsonSerializer.Deserialize<Message>(File.ReadAllText(fileToLoad+".txt"));
                return true;
            } else {
                m = null;
                return false;
            }
        }
    }
}
