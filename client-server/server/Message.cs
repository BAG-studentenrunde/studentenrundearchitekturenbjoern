using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

//Got from here: https://codinginfinite.com/multi-threaded-tcp-server-core-example-csharp/

namespace client_server
{
    public class Message
    {
        public int sender { get; set; }
        public string message { get; set; }

        public Message(int sender, string message) {
            this.sender = sender;
            this.message = message;
        }
        public Message(string message) {
            this.message = message;
        }
    }
}