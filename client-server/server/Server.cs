using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

//Got from here: https://codinginfinite.com/multi-threaded-tcp-server-core-example-csharp/

namespace client_server
{
    class Server
    {
        private Queue<Message> messagesToSend = new Queue<Message>();

        private List<TcpClient> clients = new List<TcpClient>();
        private bool endConnection = false;
        private TcpListener server = null;
        private string ip;
        private int port;

        public Server(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }

        private Thread t2;

        public void Start()
        {
            IPAddress localAddr = IPAddress.Parse(ip);
            server = new TcpListener(localAddr, port);

            t2 = new Thread(new ParameterizedThreadStart(SendDevices));
            t2.Start();

            server.Start();
            StartListener();
        }

        public void EndConnection() {
            Console.WriteLine("Server is being shut down.");

            this.endConnection = true;

            foreach(TcpClient client in clients) {
                client.Close();
            }

            if(this.server != null) {
                this.server.Stop();
            }
        }

        public void StartListener()
        {
            try
            {
                while (!endConnection)
                {
                    Console.WriteLine("Waiting for a connection...");
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Connected!");
                    Thread t = new Thread(new ParameterizedThreadStart(HandleDevice));
                    t.Start(client);
                }
            }
            catch (SocketException e)
            {
                if(!endConnection) {
                    Console.WriteLine("SocketException: {0}", e);
                }
            }
        }

        private void SendDevices(object obj)
        {
            while(!endConnection) {
                if(messagesToSend.Count > 0 && clients.Count > 0) {
                    Message toSend = this.messagesToSend.Dequeue();

                    Byte[] message = System.Text.Encoding.ASCII.GetBytes(toSend.message);
                    foreach (TcpClient client in clients) {
                        try {
                            int hash = client.GetHashCode();

                            if(hash != toSend.sender)
                            {
                                var stream = client.GetStream();
                                stream.Write(message, 0, message.Length);
                                Console.WriteLine("{1}: Sent: {0}", toSend.message, Thread.CurrentThread.ManagedThreadId);
                            }
                        } catch(Exception e) {
                            Console.WriteLine("Exception: {0}", e.ToString());
                        }
                    }
                } else {
                    Thread.Sleep(500);
                }
            }
        }

        public void HandleDevice(Object obj)
        {
            TcpClient client = (TcpClient)obj;
            int hash = client.GetHashCode();
            clients.Add(client);
            var stream = client.GetStream();
            Byte[] bytes = new Byte[256];
            int i;
            try
            {
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0 && !endConnection)
                {
                    string hex = BitConverter.ToString(bytes);
                    string data = Encoding.ASCII.GetString(bytes, 0, i);
                    Console.WriteLine("{1}: Received: {0}", data, Thread.CurrentThread.ManagedThreadId);

                    messagesToSend.Enqueue(new Message(hash, data));
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Exception: {0}", e.ToString());
                client.Close();
            }

            clients.Remove(client);
        }
    }
}