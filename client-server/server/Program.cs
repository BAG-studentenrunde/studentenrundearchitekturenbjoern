﻿using System;
using System.Threading;

namespace client_server
{
    class Program
    {
        static void Main(string[] args)
        {
            Server server = new Server("127.0.0.1", 13000);
            Thread t = new Thread(delegate ()
            {
                // replace the IP with your system IP Address...
                server.Start();
            });
            t.Start();
            
            Console.WriteLine("Server Started...! Type something in to stop.");
            Console.ReadLine();
            server.EndConnection();
        }
    }
}
