﻿using System;
using System.Threading;

namespace client_server
{
    class Program
    {
        static void Main(string[] args)
        {
            Client client1 = new Client("127.0.0.1", 13000);
            client1.Connect("Client 1 verbunden. Type End to stop!");
            client1.StartCommunication();
            
            Client client2 = new Client("127.0.0.1", 13000);
            client2.Connect("Client 2 verbunden. Type End to stop!");
            client2.StartCommunication();

            string input = "";
            do
            {
                input = Console.ReadLine();
                client1.messagesToSend.Enqueue(input);
            } while(!input.ToLower().Equals("end"));

            client1.StopCommunication();
            client2.StopCommunication();
        }
    }
}
