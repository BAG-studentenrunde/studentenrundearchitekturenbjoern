using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
namespace peerToPeer
{
    public class Client
    {
        public Queue<string> messagesToSend = new Queue<string>();
        private TcpClient client;
        private int port;
        private string serverIp;
        private bool stopCommunication = false;

        public Client(string ip, int port)
        {
            this.serverIp = ip;
            this.port = port;
        }

        public void StartCommunication()
        {
            if(client != null && client.Connected)
            {
                Thread t = new Thread(new ParameterizedThreadStart(Send));
                t.Start();
                Thread t2 = new Thread(new ParameterizedThreadStart(Receive));
                t2.Start();
            }
            else
            {
                throw new Exception("Connect to the client first");
            }
        }

        public void Connect(String message) 
        {
            try 
            {
                client = new TcpClient(serverIp, port);

                SendMessage(message);

                NetworkStream stream = client.GetStream();  
            } 
            catch (Exception e) 
            {
                Console.WriteLine("Exception: {0}", e);
            }
        }

        private void SendMessage(string message)
        {
            if(client != null && client.Connected)
            {
                NetworkStream stream = client.GetStream();
                // Translate the Message into ASCII.
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);   
                // Send the message to the connected TcpServer. 
                stream.Write(data, 0, data.Length);
                Console.WriteLine("Sent: {0}", message);    
            }
            else
            {
                throw new Exception("Connect to the client first");
            }
        }

        public void StopCommunication()
        {
            this.stopCommunication = true;
        }

        private void Send(Object obj)
        {
            while(!stopCommunication)
            {
                if(client != null && client.Connected && messagesToSend.Count > 0)
                {
                    while(messagesToSend.Count > 0)
                    {
                        SendMessage(messagesToSend.Dequeue());
                    }
                } else {
                    Thread.Sleep(500);
                }
            }

            client.Close();
            client = null;
        }

        private void Receive(Object obj)
        {
            while(!stopCommunication)
            {
                if(client != null && client.Connected)
                {
                    try 
                    {
                        NetworkStream stream = client.GetStream();
                        // Bytes Array to receive Server Response.
                        Byte[] data = new Byte[256];
                        String response = String.Empty;
                        // Read the Tcp Server Response Bytes.
                        Int32 bytes = stream.Read(data, 0, data.Length);
                        response = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                        Console.WriteLine("Received: {0}", response);
                    }
                    catch (Exception e) 
                    {
                        Console.WriteLine("Exception: {0}", e);
                    }
                } else {
                    Thread.Sleep(500);
                }
            }
        }
    }
}