﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace peerToPeer
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Client> clients = new List<Client>();
            
            Console.WriteLine("Auf welchem Port läuft dein Nachrichtenempfang?");
            string input = Console.ReadLine();
            int port = int.Parse(input);
            Server server = new Server("127.0.0.1", port);
            Thread t = new Thread(delegate ()
            {
                server.Start();
            });
            t.Start();

            Console.WriteLine("Empfang ermöglicht. Zum verbinden zu anderen peers schreibe: \"add <ip> <port>\". Sobald du verbunden bist, kannst du wie gewohnt Nachrichten verschicken. Tippe end zum beenden.");
            do
            {
                input = Console.ReadLine();
                if(input.StartsWith("add ")) {
                    string address = input.Split(" ")[1];
                    int portToConnect = int.Parse(input.Split(" ")[2]);
                    Client newClient = new Client(address, portToConnect);
                    newClient.Connect("Client verbunden. Type End to stop!");
                    newClient.StartCommunication();
                    clients.Add(newClient);
                } else if(input.ToLower() != "end") {
                    foreach(Client client in clients) {
                        client.messagesToSend.Enqueue(input);
                    }
                }
            } while(!input.ToLower().Equals("end"));

            foreach(Client client in clients) {
                client.StopCommunication();
            }
            server.EndConnection();
        }
    }
}
