using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace peerToPeer
{
    class Server
    {
        private List<TcpClient> clients = new List<TcpClient>();
        private bool endConnection = false;
        private TcpListener server;
        private string ip;
        private int port;

        public Server(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }

        public void EndConnection() {
            this.endConnection = true;

            if(this.server != null) {
                this.server.Stop();
            }
        }

        public void Start()
        {
            IPAddress localAddr = IPAddress.Parse(ip);
            server = new TcpListener(localAddr, port);

            server.Start();
            try
            {
                while (!endConnection)
                {
                    Console.WriteLine("Waiting for a connection...");
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Connected!");
                    Thread t = new Thread(new ParameterizedThreadStart(HandleDevice));
                    t.Start(client);
                }
            }
            catch (SocketException e)
            {
                if(!endConnection)
                {
                    Console.WriteLine("SocketException: {0}", e);
                    server.Stop();
                }
            }
        }

        public void HandleDevice(Object obj)
        {
            TcpClient client = (TcpClient)obj;
            int hash = client.GetHashCode();
            clients.Add(client);
            var stream = client.GetStream();
            Byte[] bytes = new Byte[256];
            int i;
            try
            {
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    string hex = BitConverter.ToString(bytes);
                    string data = Encoding.ASCII.GetString(bytes, 0, i);
                    Console.WriteLine("{1}: Received: {0}", data, Thread.CurrentThread.ManagedThreadId);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Exception: {0}", e.ToString());
                client.Close();
            }

            clients.Remove(client);
        }
    }
}