﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;
using newsNs;
using static newsNs.News;

namespace client
{
    class Program
    {
        private static NewsClient client;
        private static string name;
        private static int currentMessageId;

        static async Task Main(string[] args)
        {
            name = "TestClient 1";
            // The port number(5001) must match the port of the gRPC server.
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            client =  new News.NewsClient(channel);

            Console.WriteLine("Write messages to get messages from number x and up");
            Console.WriteLine("Write newmessage to generate a new message");
            Console.WriteLine("Write end to stop");
            
            string next = "messages";
            do {
                if(next == "messages") {
                    Console.WriteLine("Starting what ID?");
                    int number = Int32.Parse(Console.ReadLine());
                    AsyncServerStreamingCall<NewsMessage> news = client.GetNews(new NewsRequest() {
                        FromId = number
                    });

                    IAsyncStreamReader<NewsMessage> respStream = news.ResponseStream;
                    while (await respStream.MoveNext())
                    {
                        Console.WriteLine(respStream.Current.Name);
                        Console.WriteLine(respStream.Current.Message);
                    }
                } else if (next == "newmessage") {
                    Console.WriteLine("Message name");
                    string name = Console.ReadLine();
                    Console.WriteLine("Message message");
                    string message = Console.ReadLine();

                    await client.SendMessageAsync(new NewsMessage() {
                        Id = 0,
                        Message = message,
                        Name = name
                    });
                }

                Console.WriteLine("What to do next?");
                next = Console.ReadLine();
            } while(next != "end");
        }
    }
}
