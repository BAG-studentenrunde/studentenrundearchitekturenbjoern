var PROTO_PATH = __dirname + '/../news.proto';

const readline = require('readline');
const fs = require('fs');
var parseArgs = require('minimist');
var grpc = require('@grpc/grpc-js');
var grpc_xds = require('@grpc/grpc-js-xds');
grpc_xds.register();

var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
var grpcProtoDefinition = grpc.loadPackageDefinition(packageDefinition).grpc;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
const ans = rl.question("What do you want to do? messages? newmessage? end? ", whatToDo);

function whatToDo(answer) {
    if(answer === "messages") {
        console.log("message");
    } else if(answer === "newmessage") {
        const ans = rl.question("Which message?", sendMessage);
    } else if(answer === "end") {
        end = true;
    }
}

function sendMessage(message) {
    var target = 'localhost:5001';
    var client = new grpcProtoDefinition.News(target,
                                         grpc.credentials.createSsl(fs.readFileSync('./localCertificate.crt')));
  
    client.SendMessage({ name: "Awesome", message: message, id: 1 }, function(err, response) {
      if (err) throw err;
      console.log('Hello:', response.id);
      client.close();
    });
}