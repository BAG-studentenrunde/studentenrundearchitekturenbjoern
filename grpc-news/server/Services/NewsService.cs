using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace newsNs
{
    public class NewsService : News.NewsBase
    {
        private readonly ILogger<NewsService> _logger;
        public NewsService(ILogger<NewsService> logger)
        {
            _logger = logger;
        }

        public override Task<NewsMessage> SendMessage(NewsMessage request, ServerCallContext context)
        {
            List<NewsMessage> messages = this.loadNews();

            int newId = 1;
            if(messages.Count > 0) {
                newId = messages.Select(x => x.Id).Max() + 1;
            }

            request.Id = newId;
            messages.Add(request);

            saveNews(messages);

            return Task.FromResult(request);

        }

        public override Task GetNews(NewsRequest request, IServerStreamWriter<NewsMessage> responseStream, ServerCallContext context)
        {
            List<NewsMessage> messages = loadNews();
            Task result = null;

            foreach(NewsMessage message in messages) {
                result = responseStream.WriteAsync(message);
            }

            return result;
        }

        private List<NewsMessage> loadNews() {
            List<NewsMessage> result = new List<NewsMessage>();

            if(File.Exists("../news.json")) {
                string serialized = File.ReadAllText("../news.json");
                result = JsonSerializer.Deserialize<List<NewsMessage>>(serialized);
            }

            return result;
        }

        private void saveNews(List<NewsMessage> messages) {
            Console.WriteLine("Start load messages");
            string serialized = JsonSerializer.Serialize(messages);
            File.WriteAllText("../news.json", serialized);
        }
    }
}
